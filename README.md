# Git Workflow

This repository is created to document my learning journey about Git workflow. I will be updating this readme file as I learn more.

## Learning Resources

- [How to Use Git and Git Workflows – a Practical Guide](https://www.freecodecamp.org/news/practical-git-and-git-workflows/)
- [Gitflow Workflow by Atlassian](https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow)
- [What is the best Git branch strategy?](https://www.gitkraken.com/learn/git/best-practices/git-branch-strategy)
- [Git Branch Merging Best Practices](https://gist.github.com/calaway/ea880263b0c0495bb00ee877f001dc59)
- [5 Git workflows and branching strategy you can use to improve your development process](https://rovitpm.com/5-git-workflows-to-improve-development/)
- [5 Git workflow best practices you've got to use [2021]](https://raygun.com/blog/git-workflow/)
- [Git Workflow Mudah Kok!](https://dev.to/mandaputtra/git-workflow-mudah-kok-1d5g)

## About the Repository

This repository is used for learning purposes only. I will be experimenting with different Git workflows and commands to better understand version control and collaboration with others.

## Disclaimer

The content and files in this repository are for learning purposes only. Any code or information provided is not intended for production use.
